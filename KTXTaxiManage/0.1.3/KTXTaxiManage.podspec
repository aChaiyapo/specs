Pod::Spec.new do |s|
  s.name         = 'KTXTaxiManage'
  s.version      = '0.1.3'
  s.summary      = 'Booking a taxi through connect API.'
  s.author       = {'supamard' => 'kwan_st82@hotmail.com'}
  s.license      = 'MIT'
  s.homepage 	   = 'https://bitbucket.org/supamard_kwan/ktxtaximanage'
  s.source       = {:git => "https://infostant@bitbucket.org/infostant/ktxtaximanage-renew.git", :tag =>'0.1.3'}
  s.platform     = :ios, '5.0'
  s.source_files = 'TaxiManageClass/TaxiManage/*.{h,m}'
  s.resources    = ['TaxiManageClass/resouce/*.storyboard','TaxiManageClass/resouce/*.png']
  s.exclude_files = 'TaxiManageClass/resouce/Default-568h@2x.png','TaxiManageClass/resouce/Default.png','TaxiManageClass/resouce/Default@2x.png'
  s.frameworks	 = 'CoreLocation', 'MapKit'
  s.requires_arc = true
  s.dependency 'AFNetworking', '~> 1.3.1'
  s.dependency 'MZFormSheetController'
  s.dependency 'SIAlertView'
  s.dependency 'MBProgressHUD'
  s.dependency 'GKImagePicker'
  s.dependency 'DMConnexPluginProtocol'
end
