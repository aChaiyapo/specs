Pod::Spec.new do |s|
  s.name     = 'DMContentCreator'
  s.version  = '1.1.0'
  s.license  = 'Commercial'
  s.summary  = 'Content creator plugin for DMConnex plugin'
  s.homepage = 'http://www.infostant.com/'
  s.author   = { 'Nattawut Singhchai' => 'wut@2bsimple.com' }

  s.source   = { :git => 'https://infostant@bitbucket.org/infostant/dmcontentcreator.git',:tag => '1.1.0'}

  s.platform = :ios
  s.ios.deployment_target = '6.0'
  s.source_files = 'DMContentCreator/Classes/*.{h,m}','DMContentCreator/Classes/**/*.{h,m}'

  s.resources = 'DMContentCreator/Resources/*.{storyboard,png,bundle}'
  #s.exclude_files = 'Graphics/Default-568h@2x.png'
  s.requires_arc = true
  s.frameworks = 'MapKit'
    s.dependency 'UIImage-Resize'
    s.dependency 'SIAlertView'
    s.dependency 'LAUtilitiesStaticLib'
    s.dependency 'BlocksKit'
    s.dependency 'ALActionBlocks'
    s.dependency 'WTGlyphFontSet'
    s.dependency 'WTGlyphFontSet/fontawesome'
    s.dependency 'HCYoutubeParser'
    s.dependency 'FoundationExtension'
    s.dependency 'RNGridMenu'
    s.dependency 'MBProgressHUD'
    s.dependency 'iOS7Colors'
    s.dependency 'CZPhotoPickerController'
    s.dependency 'DMReorderTableView'
    s.dependency 'AFNetworking','~>1.3.3'
    s.dependency 'FXBlurView'
end
